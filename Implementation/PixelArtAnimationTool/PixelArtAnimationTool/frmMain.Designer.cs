﻿namespace PixelArtAnimationTool
{
	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.lblNoise = new System.Windows.Forms.Label();
			this.lblSize = new System.Windows.Forms.Label();
			this.nudPencilNoise = new System.Windows.Forms.NumericUpDown();
			this.nudPencilSize = new System.Windows.Forms.NumericUpDown();
			this.pbxPreviousColor5 = new System.Windows.Forms.PictureBox();
			this.pbxPreviousColor1 = new System.Windows.Forms.PictureBox();
			this.btnCopyFrame = new System.Windows.Forms.Button();
			this.lblFPS = new System.Windows.Forms.Label();
			this.nudFPS = new System.Windows.Forms.NumericUpDown();
			this.cbxLoop = new System.Windows.Forms.CheckBox();
			this.btnNext = new System.Windows.Forms.Button();
			this.btnPrevious = new System.Windows.Forms.Button();
			this.btnPlayPause = new System.Windows.Forms.Button();
			this.pbxCurrentColor = new System.Windows.Forms.PictureBox();
			this.pbxPreviousColor2 = new System.Windows.Forms.PictureBox();
			this.pbxPreviousColor6 = new System.Windows.Forms.PictureBox();
			this.pbxPreviousColor3 = new System.Windows.Forms.PictureBox();
			this.pbxPreviousColor7 = new System.Windows.Forms.PictureBox();
			this.pbxPreviousColor4 = new System.Windows.Forms.PictureBox();
			this.pbxPreviousColor8 = new System.Windows.Forms.PictureBox();
			this.btnEraser = new System.Windows.Forms.Button();
			this.btnPencil = new System.Windows.Forms.Button();
			this.btnZoom = new System.Windows.Forms.Button();
			this.btnSelect = new System.Windows.Forms.Button();
			this.btnDarken = new System.Windows.Forms.Button();
			this.btnLighten = new System.Windows.Forms.Button();
			this.btnCircle = new System.Windows.Forms.Button();
			this.btnLine = new System.Windows.Forms.Button();
			this.btnFill = new System.Windows.Forms.Button();
			this.btnEyedropper = new System.Windows.Forms.Button();
			this.pbxCanvas = new System.Windows.Forms.PictureBox();
			this.MenuStrip = new System.Windows.Forms.MenuStrip();
			this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiOpenPixelAnimation = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiOpenTracingImage = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiOpenIndividualFrame = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiSaveAsSpriteSheet = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiSaveAsFolder = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiSelection = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiCut = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiPaste = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiRotate = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiRotate90 = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiRotate180 = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiRotate270 = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiFlip = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiFlipHorizontal = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiFlipVertical = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiHelp = new System.Windows.Forms.ToolStripMenuItem();
			this.Timer = new System.Windows.Forms.Timer(this.components);
			this.dlgColor = new System.Windows.Forms.ColorDialog();
			this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
			this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
			this.lblFrameTracker = new System.Windows.Forms.Label();
			this.tbrFrame = new System.Windows.Forms.TrackBar();
			this.lblFrame = new System.Windows.Forms.Label();
			this.lblZoom = new System.Windows.Forms.Label();
			this.nudZoom = new System.Windows.Forms.NumericUpDown();
			this.cbxFillCorners = new System.Windows.Forms.CheckBox();
			this.nudEraserSize = new System.Windows.Forms.NumericUpDown();
			this.cbxClearArea = new System.Windows.Forms.CheckBox();
			this.nudLightenSize = new System.Windows.Forms.NumericUpDown();
			this.nudDarkenSize = new System.Windows.Forms.NumericUpDown();
			this.nudLightenIntensity = new System.Windows.Forms.NumericUpDown();
			this.nudDarkenIntensity = new System.Windows.Forms.NumericUpDown();
			this.lblIntensity = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.nudPencilNoise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudPencilSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudFPS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxCurrentColor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxCanvas)).BeginInit();
			this.MenuStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbrFrame)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudZoom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudEraserSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudLightenSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudDarkenSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudLightenIntensity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudDarkenIntensity)).BeginInit();
			this.SuspendLayout();
			// 
			// lblNoise
			// 
			this.lblNoise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblNoise.AutoSize = true;
			this.lblNoise.Location = new System.Drawing.Point(565, 76);
			this.lblNoise.Name = "lblNoise";
			this.lblNoise.Size = new System.Drawing.Size(37, 13);
			this.lblNoise.TabIndex = 67;
			this.lblNoise.Text = "Noise:";
			// 
			// lblSize
			// 
			this.lblSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblSize.AutoSize = true;
			this.lblSize.Location = new System.Drawing.Point(565, 53);
			this.lblSize.Name = "lblSize";
			this.lblSize.Size = new System.Drawing.Size(30, 13);
			this.lblSize.TabIndex = 66;
			this.lblSize.Text = "Size:";
			// 
			// nudPencilNoise
			// 
			this.nudPencilNoise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nudPencilNoise.Location = new System.Drawing.Point(608, 74);
			this.nudPencilNoise.Name = "nudPencilNoise";
			this.nudPencilNoise.Size = new System.Drawing.Size(86, 20);
			this.nudPencilNoise.TabIndex = 65;
			// 
			// nudPencilSize
			// 
			this.nudPencilSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nudPencilSize.Location = new System.Drawing.Point(608, 44);
			this.nudPencilSize.Name = "nudPencilSize";
			this.nudPencilSize.Size = new System.Drawing.Size(86, 20);
			this.nudPencilSize.TabIndex = 64;
			// 
			// pbxPreviousColor5
			// 
			this.pbxPreviousColor5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor5.BackColor = System.Drawing.Color.Green;
			this.pbxPreviousColor5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor5.Location = new System.Drawing.Point(435, 70);
			this.pbxPreviousColor5.Name = "pbxPreviousColor5";
			this.pbxPreviousColor5.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor5.TabIndex = 63;
			this.pbxPreviousColor5.TabStop = false;
			this.pbxPreviousColor5.Click += new System.EventHandler(this.pbxPreviousColor5_Click);
			// 
			// pbxPreviousColor1
			// 
			this.pbxPreviousColor1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor1.BackColor = System.Drawing.Color.Red;
			this.pbxPreviousColor1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor1.Location = new System.Drawing.Point(435, 41);
			this.pbxPreviousColor1.Name = "pbxPreviousColor1";
			this.pbxPreviousColor1.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor1.TabIndex = 62;
			this.pbxPreviousColor1.TabStop = false;
			this.pbxPreviousColor1.Click += new System.EventHandler(this.pbxPreviousColor1_Click);
			// 
			// btnCopyFrame
			// 
			this.btnCopyFrame.Location = new System.Drawing.Point(144, 39);
			this.btnCopyFrame.Name = "btnCopyFrame";
			this.btnCopyFrame.Size = new System.Drawing.Size(60, 56);
			this.btnCopyFrame.TabIndex = 61;
			this.btnCopyFrame.Text = "Copy Previous Frame";
			this.btnCopyFrame.UseVisualStyleBackColor = true;
			this.btnCopyFrame.Click += new System.EventHandler(this.btnCopyFrame_Click);
			// 
			// lblFPS
			// 
			this.lblFPS.AutoSize = true;
			this.lblFPS.Location = new System.Drawing.Point(210, 47);
			this.lblFPS.Name = "lblFPS";
			this.lblFPS.Size = new System.Drawing.Size(103, 13);
			this.lblFPS.TabIndex = 60;
			this.lblFPS.Text = "Frames Per Second:";
			// 
			// nudFPS
			// 
			this.nudFPS.Location = new System.Drawing.Point(315, 45);
			this.nudFPS.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudFPS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nudFPS.Name = "nudFPS";
			this.nudFPS.Size = new System.Drawing.Size(47, 20);
			this.nudFPS.TabIndex = 59;
			this.nudFPS.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// cbxLoop
			// 
			this.cbxLoop.AutoSize = true;
			this.cbxLoop.Checked = true;
			this.cbxLoop.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbxLoop.Location = new System.Drawing.Point(213, 70);
			this.cbxLoop.Name = "cbxLoop";
			this.cbxLoop.Size = new System.Drawing.Size(97, 17);
			this.cbxLoop.TabIndex = 58;
			this.cbxLoop.Text = "Loop Playback";
			this.cbxLoop.UseVisualStyleBackColor = true;
			// 
			// btnNext
			// 
			this.btnNext.Location = new System.Drawing.Point(78, 70);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(60, 25);
			this.btnNext.TabIndex = 57;
			this.btnNext.Text = "Next";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// btnPrevious
			// 
			this.btnPrevious.Location = new System.Drawing.Point(12, 70);
			this.btnPrevious.Name = "btnPrevious";
			this.btnPrevious.Size = new System.Drawing.Size(60, 25);
			this.btnPrevious.TabIndex = 55;
			this.btnPrevious.Text = "Previous";
			this.btnPrevious.UseVisualStyleBackColor = true;
			this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
			// 
			// btnPlayPause
			// 
			this.btnPlayPause.Location = new System.Drawing.Point(12, 41);
			this.btnPlayPause.Name = "btnPlayPause";
			this.btnPlayPause.Size = new System.Drawing.Size(126, 25);
			this.btnPlayPause.TabIndex = 54;
			this.btnPlayPause.Text = "Play";
			this.btnPlayPause.UseVisualStyleBackColor = true;
			this.btnPlayPause.Click += new System.EventHandler(this.btnPlayPause_Click);
			// 
			// pbxCurrentColor
			// 
			this.pbxCurrentColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxCurrentColor.BackColor = System.Drawing.Color.Black;
			this.pbxCurrentColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxCurrentColor.Location = new System.Drawing.Point(373, 41);
			this.pbxCurrentColor.Name = "pbxCurrentColor";
			this.pbxCurrentColor.Size = new System.Drawing.Size(54, 54);
			this.pbxCurrentColor.TabIndex = 53;
			this.pbxCurrentColor.TabStop = false;
			this.pbxCurrentColor.MouseClick += new System.Windows.Forms.MouseEventHandler(this.choose_color);
			// 
			// pbxPreviousColor2
			// 
			this.pbxPreviousColor2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor2.BackColor = System.Drawing.Color.Orange;
			this.pbxPreviousColor2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor2.Location = new System.Drawing.Point(466, 41);
			this.pbxPreviousColor2.Name = "pbxPreviousColor2";
			this.pbxPreviousColor2.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor2.TabIndex = 52;
			this.pbxPreviousColor2.TabStop = false;
			this.pbxPreviousColor2.Click += new System.EventHandler(this.pbxPreviousColor2_Click);
			// 
			// pbxPreviousColor6
			// 
			this.pbxPreviousColor6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor6.BackColor = System.Drawing.Color.Blue;
			this.pbxPreviousColor6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor6.Location = new System.Drawing.Point(466, 70);
			this.pbxPreviousColor6.Name = "pbxPreviousColor6";
			this.pbxPreviousColor6.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor6.TabIndex = 51;
			this.pbxPreviousColor6.TabStop = false;
			this.pbxPreviousColor6.Click += new System.EventHandler(this.pbxPreviousColor6_Click);
			// 
			// pbxPreviousColor3
			// 
			this.pbxPreviousColor3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor3.BackColor = System.Drawing.Color.Yellow;
			this.pbxPreviousColor3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor3.Location = new System.Drawing.Point(497, 41);
			this.pbxPreviousColor3.Name = "pbxPreviousColor3";
			this.pbxPreviousColor3.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor3.TabIndex = 50;
			this.pbxPreviousColor3.TabStop = false;
			this.pbxPreviousColor3.Click += new System.EventHandler(this.pbxPreviousColor3_Click);
			// 
			// pbxPreviousColor7
			// 
			this.pbxPreviousColor7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor7.BackColor = System.Drawing.Color.Purple;
			this.pbxPreviousColor7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor7.Location = new System.Drawing.Point(497, 70);
			this.pbxPreviousColor7.Name = "pbxPreviousColor7";
			this.pbxPreviousColor7.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor7.TabIndex = 49;
			this.pbxPreviousColor7.TabStop = false;
			this.pbxPreviousColor7.Click += new System.EventHandler(this.pbxPreviousColor7_Click);
			// 
			// pbxPreviousColor4
			// 
			this.pbxPreviousColor4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor4.BackColor = System.Drawing.Color.Silver;
			this.pbxPreviousColor4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor4.Location = new System.Drawing.Point(528, 41);
			this.pbxPreviousColor4.Name = "pbxPreviousColor4";
			this.pbxPreviousColor4.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor4.TabIndex = 48;
			this.pbxPreviousColor4.TabStop = false;
			this.pbxPreviousColor4.Click += new System.EventHandler(this.pbxPreviousColor4_Click);
			// 
			// pbxPreviousColor8
			// 
			this.pbxPreviousColor8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxPreviousColor8.BackColor = System.Drawing.Color.White;
			this.pbxPreviousColor8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbxPreviousColor8.Location = new System.Drawing.Point(528, 70);
			this.pbxPreviousColor8.Name = "pbxPreviousColor8";
			this.pbxPreviousColor8.Size = new System.Drawing.Size(25, 25);
			this.pbxPreviousColor8.TabIndex = 47;
			this.pbxPreviousColor8.TabStop = false;
			this.pbxPreviousColor8.Click += new System.EventHandler(this.pbxPreviousColor8_Click);
			// 
			// btnEraser
			// 
			this.btnEraser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEraser.Image = ((System.Drawing.Image)(resources.GetObject("btnEraser.Image")));
			this.btnEraser.Location = new System.Drawing.Point(634, 106);
			this.btnEraser.Name = "btnEraser";
			this.btnEraser.Size = new System.Drawing.Size(60, 60);
			this.btnEraser.TabIndex = 45;
			this.btnEraser.UseVisualStyleBackColor = true;
			this.btnEraser.Click += new System.EventHandler(this.btnEraser_Click);
			// 
			// btnPencil
			// 
			this.btnPencil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPencil.Image = ((System.Drawing.Image)(resources.GetObject("btnPencil.Image")));
			this.btnPencil.Location = new System.Drawing.Point(568, 106);
			this.btnPencil.Name = "btnPencil";
			this.btnPencil.Size = new System.Drawing.Size(60, 60);
			this.btnPencil.TabIndex = 44;
			this.btnPencil.UseVisualStyleBackColor = true;
			this.btnPencil.Click += new System.EventHandler(this.btnPencil_Click);
			// 
			// btnZoom
			// 
			this.btnZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnZoom.Image = ((System.Drawing.Image)(resources.GetObject("btnZoom.Image")));
			this.btnZoom.Location = new System.Drawing.Point(634, 370);
			this.btnZoom.Name = "btnZoom";
			this.btnZoom.Size = new System.Drawing.Size(60, 60);
			this.btnZoom.TabIndex = 43;
			this.btnZoom.UseVisualStyleBackColor = true;
			this.btnZoom.Click += new System.EventHandler(this.btnZoom_Click);
			// 
			// btnSelect
			// 
			this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
			this.btnSelect.Location = new System.Drawing.Point(568, 370);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(60, 60);
			this.btnSelect.TabIndex = 42;
			this.btnSelect.UseVisualStyleBackColor = true;
			this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
			// 
			// btnDarken
			// 
			this.btnDarken.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDarken.Image = ((System.Drawing.Image)(resources.GetObject("btnDarken.Image")));
			this.btnDarken.Location = new System.Drawing.Point(634, 304);
			this.btnDarken.Name = "btnDarken";
			this.btnDarken.Size = new System.Drawing.Size(60, 60);
			this.btnDarken.TabIndex = 41;
			this.btnDarken.UseVisualStyleBackColor = true;
			this.btnDarken.Click += new System.EventHandler(this.btnDarken_Click);
			// 
			// btnLighten
			// 
			this.btnLighten.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnLighten.Image = ((System.Drawing.Image)(resources.GetObject("btnLighten.Image")));
			this.btnLighten.Location = new System.Drawing.Point(568, 304);
			this.btnLighten.Name = "btnLighten";
			this.btnLighten.Size = new System.Drawing.Size(60, 60);
			this.btnLighten.TabIndex = 40;
			this.btnLighten.UseVisualStyleBackColor = true;
			this.btnLighten.Click += new System.EventHandler(this.btnLighten_Click);
			// 
			// btnCircle
			// 
			this.btnCircle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCircle.Image = ((System.Drawing.Image)(resources.GetObject("btnCircle.Image")));
			this.btnCircle.Location = new System.Drawing.Point(634, 238);
			this.btnCircle.Name = "btnCircle";
			this.btnCircle.Size = new System.Drawing.Size(60, 60);
			this.btnCircle.TabIndex = 39;
			this.btnCircle.UseVisualStyleBackColor = true;
			this.btnCircle.Click += new System.EventHandler(this.btnCircle_Click);
			// 
			// btnLine
			// 
			this.btnLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnLine.Image = ((System.Drawing.Image)(resources.GetObject("btnLine.Image")));
			this.btnLine.Location = new System.Drawing.Point(568, 238);
			this.btnLine.Name = "btnLine";
			this.btnLine.Size = new System.Drawing.Size(60, 60);
			this.btnLine.TabIndex = 38;
			this.btnLine.UseVisualStyleBackColor = true;
			this.btnLine.Click += new System.EventHandler(this.btnLine_Click);
			// 
			// btnFill
			// 
			this.btnFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnFill.Image = ((System.Drawing.Image)(resources.GetObject("btnFill.Image")));
			this.btnFill.Location = new System.Drawing.Point(634, 172);
			this.btnFill.Name = "btnFill";
			this.btnFill.Size = new System.Drawing.Size(60, 60);
			this.btnFill.TabIndex = 37;
			this.btnFill.UseVisualStyleBackColor = true;
			this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
			// 
			// btnEyedropper
			// 
			this.btnEyedropper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEyedropper.Image = ((System.Drawing.Image)(resources.GetObject("btnEyedropper.Image")));
			this.btnEyedropper.Location = new System.Drawing.Point(568, 172);
			this.btnEyedropper.Name = "btnEyedropper";
			this.btnEyedropper.Size = new System.Drawing.Size(60, 60);
			this.btnEyedropper.TabIndex = 36;
			this.btnEyedropper.UseVisualStyleBackColor = true;
			this.btnEyedropper.Click += new System.EventHandler(this.btnEyedropper_Click);
			// 
			// pbxCanvas
			// 
			this.pbxCanvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pbxCanvas.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.pbxCanvas.Cursor = System.Windows.Forms.Cursors.Cross;
			this.pbxCanvas.Location = new System.Drawing.Point(13, 106);
			this.pbxCanvas.Name = "pbxCanvas";
			this.pbxCanvas.Size = new System.Drawing.Size(540, 326);
			this.pbxCanvas.TabIndex = 35;
			this.pbxCanvas.TabStop = false;
			this.pbxCanvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Draw);
			this.pbxCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.start_drawing);
			this.pbxCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Draw);
			this.pbxCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stop_drawing);
			// 
			// MenuStrip
			// 
			this.MenuStrip.BackColor = System.Drawing.SystemColors.ControlLight;
			this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiSelection,
            this.tsmiHelp});
			this.MenuStrip.Location = new System.Drawing.Point(0, 0);
			this.MenuStrip.Name = "MenuStrip";
			this.MenuStrip.Size = new System.Drawing.Size(709, 24);
			this.MenuStrip.TabIndex = 46;
			this.MenuStrip.Text = "MenuStrip";
			// 
			// tsmiFile
			// 
			this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNew,
            this.tsmiOpen,
            this.tsmiSave,
            this.tsmiSaveAs});
			this.tsmiFile.Name = "tsmiFile";
			this.tsmiFile.Size = new System.Drawing.Size(37, 20);
			this.tsmiFile.Text = "File";
			// 
			// tsmiNew
			// 
			this.tsmiNew.Name = "tsmiNew";
			this.tsmiNew.Size = new System.Drawing.Size(114, 22);
			this.tsmiNew.Text = "New";
			// 
			// tsmiOpen
			// 
			this.tsmiOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOpenPixelAnimation,
            this.tsmiOpenTracingImage,
            this.tsmiOpenIndividualFrame});
			this.tsmiOpen.Name = "tsmiOpen";
			this.tsmiOpen.Size = new System.Drawing.Size(114, 22);
			this.tsmiOpen.Text = "Open";
			// 
			// tsmiOpenPixelAnimation
			// 
			this.tsmiOpenPixelAnimation.Name = "tsmiOpenPixelAnimation";
			this.tsmiOpenPixelAnimation.Size = new System.Drawing.Size(162, 22);
			this.tsmiOpenPixelAnimation.Text = "Pixel Animation";
			// 
			// tsmiOpenTracingImage
			// 
			this.tsmiOpenTracingImage.Name = "tsmiOpenTracingImage";
			this.tsmiOpenTracingImage.Size = new System.Drawing.Size(162, 22);
			this.tsmiOpenTracingImage.Text = "Tracing Image";
			// 
			// tsmiOpenIndividualFrame
			// 
			this.tsmiOpenIndividualFrame.Name = "tsmiOpenIndividualFrame";
			this.tsmiOpenIndividualFrame.Size = new System.Drawing.Size(162, 22);
			this.tsmiOpenIndividualFrame.Text = "Individual Frame";
			// 
			// tsmiSave
			// 
			this.tsmiSave.Name = "tsmiSave";
			this.tsmiSave.Size = new System.Drawing.Size(114, 22);
			this.tsmiSave.Text = "Save";
			// 
			// tsmiSaveAs
			// 
			this.tsmiSaveAs.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSaveAsSpriteSheet,
            this.tsmiSaveAsFolder});
			this.tsmiSaveAs.Name = "tsmiSaveAs";
			this.tsmiSaveAs.Size = new System.Drawing.Size(114, 22);
			this.tsmiSaveAs.Text = "Save As";
			// 
			// tsmiSaveAsSpriteSheet
			// 
			this.tsmiSaveAsSpriteSheet.Name = "tsmiSaveAsSpriteSheet";
			this.tsmiSaveAsSpriteSheet.Size = new System.Drawing.Size(136, 22);
			this.tsmiSaveAsSpriteSheet.Text = "Sprite Sheet";
			this.tsmiSaveAsSpriteSheet.Click += new System.EventHandler(this.tsmiSaveAsSpriteSheet_Click);
			// 
			// tsmiSaveAsFolder
			// 
			this.tsmiSaveAsFolder.Name = "tsmiSaveAsFolder";
			this.tsmiSaveAsFolder.Size = new System.Drawing.Size(136, 22);
			this.tsmiSaveAsFolder.Text = "Folder";
			this.tsmiSaveAsFolder.Click += new System.EventHandler(this.tsmiSaveAsFolder_Click);
			// 
			// tsmiSelection
			// 
			this.tsmiSelection.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCut,
            this.tsmiCopy,
            this.tsmiPaste,
            this.tsmiRotate,
            this.tsmiFlip});
			this.tsmiSelection.Name = "tsmiSelection";
			this.tsmiSelection.Size = new System.Drawing.Size(67, 20);
			this.tsmiSelection.Text = "Selection";
			// 
			// tsmiCut
			// 
			this.tsmiCut.Name = "tsmiCut";
			this.tsmiCut.Size = new System.Drawing.Size(108, 22);
			this.tsmiCut.Text = "Cut";
			// 
			// tsmiCopy
			// 
			this.tsmiCopy.Name = "tsmiCopy";
			this.tsmiCopy.Size = new System.Drawing.Size(108, 22);
			this.tsmiCopy.Text = "Copy";
			// 
			// tsmiPaste
			// 
			this.tsmiPaste.Name = "tsmiPaste";
			this.tsmiPaste.Size = new System.Drawing.Size(108, 22);
			this.tsmiPaste.Text = "Paste";
			// 
			// tsmiRotate
			// 
			this.tsmiRotate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRotate90,
            this.tsmiRotate180,
            this.tsmiRotate270});
			this.tsmiRotate.Name = "tsmiRotate";
			this.tsmiRotate.Size = new System.Drawing.Size(108, 22);
			this.tsmiRotate.Text = "Rotate";
			// 
			// tsmiRotate90
			// 
			this.tsmiRotate90.Name = "tsmiRotate90";
			this.tsmiRotate90.Size = new System.Drawing.Size(97, 22);
			this.tsmiRotate90.Text = "90°";
			// 
			// tsmiRotate180
			// 
			this.tsmiRotate180.Name = "tsmiRotate180";
			this.tsmiRotate180.Size = new System.Drawing.Size(97, 22);
			this.tsmiRotate180.Text = "180°";
			// 
			// tsmiRotate270
			// 
			this.tsmiRotate270.Name = "tsmiRotate270";
			this.tsmiRotate270.Size = new System.Drawing.Size(97, 22);
			this.tsmiRotate270.Text = "270°";
			// 
			// tsmiFlip
			// 
			this.tsmiFlip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFlipHorizontal,
            this.tsmiFlipVertical});
			this.tsmiFlip.Name = "tsmiFlip";
			this.tsmiFlip.Size = new System.Drawing.Size(108, 22);
			this.tsmiFlip.Text = "Flip";
			// 
			// tsmiFlipHorizontal
			// 
			this.tsmiFlipHorizontal.Name = "tsmiFlipHorizontal";
			this.tsmiFlipHorizontal.Size = new System.Drawing.Size(129, 22);
			this.tsmiFlipHorizontal.Text = "Horizontal";
			// 
			// tsmiFlipVertical
			// 
			this.tsmiFlipVertical.Name = "tsmiFlipVertical";
			this.tsmiFlipVertical.Size = new System.Drawing.Size(129, 22);
			this.tsmiFlipVertical.Text = "Vertical";
			// 
			// tsmiHelp
			// 
			this.tsmiHelp.Name = "tsmiHelp";
			this.tsmiHelp.Size = new System.Drawing.Size(44, 20);
			this.tsmiHelp.Text = "Help";
			// 
			// Timer
			// 
			this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
			// 
			// dlgOpenFile
			// 
			this.dlgOpenFile.FileName = "openFileDialog1";
			// 
			// lblFrameTracker
			// 
			this.lblFrameTracker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lblFrameTracker.Location = new System.Drawing.Point(559, 462);
			this.lblFrameTracker.Name = "lblFrameTracker";
			this.lblFrameTracker.Size = new System.Drawing.Size(69, 18);
			this.lblFrameTracker.TabIndex = 68;
			this.lblFrameTracker.Text = "1/1";
			this.lblFrameTracker.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbrFrame
			// 
			this.tbrFrame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tbrFrame.Location = new System.Drawing.Point(13, 438);
			this.tbrFrame.Maximum = 1;
			this.tbrFrame.Minimum = 1;
			this.tbrFrame.Name = "tbrFrame";
			this.tbrFrame.Size = new System.Drawing.Size(540, 45);
			this.tbrFrame.TabIndex = 71;
			this.tbrFrame.Value = 1;
			this.tbrFrame.Scroll += new System.EventHandler(this.tbrFrame_Scroll);
			// 
			// lblFrame
			// 
			this.lblFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lblFrame.AutoSize = true;
			this.lblFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFrame.Location = new System.Drawing.Point(583, 438);
			this.lblFrame.Name = "lblFrame";
			this.lblFrame.Size = new System.Drawing.Size(45, 13);
			this.lblFrame.TabIndex = 72;
			this.lblFrame.Text = "Frame:";
			// 
			// lblZoom
			// 
			this.lblZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lblZoom.AutoSize = true;
			this.lblZoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblZoom.Location = new System.Drawing.Point(652, 438);
			this.lblZoom.Name = "lblZoom";
			this.lblZoom.Size = new System.Drawing.Size(42, 13);
			this.lblZoom.TabIndex = 73;
			this.lblZoom.Text = "Zoom:";
			// 
			// nudZoom
			// 
			this.nudZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.nudZoom.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.nudZoom.Location = new System.Drawing.Point(634, 460);
			this.nudZoom.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudZoom.Name = "nudZoom";
			this.nudZoom.Size = new System.Drawing.Size(59, 20);
			this.nudZoom.TabIndex = 74;
			this.nudZoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.nudZoom.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// cbxFillCorners
			// 
			this.cbxFillCorners.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbxFillCorners.AutoSize = true;
			this.cbxFillCorners.Location = new System.Drawing.Point(568, 53);
			this.cbxFillCorners.Name = "cbxFillCorners";
			this.cbxFillCorners.Size = new System.Drawing.Size(126, 17);
			this.cbxFillCorners.TabIndex = 75;
			this.cbxFillCorners.Text = "Fill Through Corners?";
			this.cbxFillCorners.UseVisualStyleBackColor = true;
			this.cbxFillCorners.Visible = false;
			// 
			// nudEraserSize
			// 
			this.nudEraserSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nudEraserSize.Location = new System.Drawing.Point(608, 44);
			this.nudEraserSize.Name = "nudEraserSize";
			this.nudEraserSize.Size = new System.Drawing.Size(86, 20);
			this.nudEraserSize.TabIndex = 76;
			this.nudEraserSize.Visible = false;
			// 
			// cbxClearArea
			// 
			this.cbxClearArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbxClearArea.AutoSize = true;
			this.cbxClearArea.Location = new System.Drawing.Point(568, 76);
			this.cbxClearArea.Name = "cbxClearArea";
			this.cbxClearArea.Size = new System.Drawing.Size(75, 17);
			this.cbxClearArea.TabIndex = 77;
			this.cbxClearArea.Text = "Clear Area";
			this.cbxClearArea.UseVisualStyleBackColor = true;
			this.cbxClearArea.Visible = false;
			// 
			// nudLightenSize
			// 
			this.nudLightenSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nudLightenSize.Location = new System.Drawing.Point(608, 44);
			this.nudLightenSize.Name = "nudLightenSize";
			this.nudLightenSize.Size = new System.Drawing.Size(86, 20);
			this.nudLightenSize.TabIndex = 78;
			this.nudLightenSize.Visible = false;
			// 
			// nudDarkenSize
			// 
			this.nudDarkenSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nudDarkenSize.Location = new System.Drawing.Point(608, 44);
			this.nudDarkenSize.Name = "nudDarkenSize";
			this.nudDarkenSize.Size = new System.Drawing.Size(86, 20);
			this.nudDarkenSize.TabIndex = 79;
			this.nudDarkenSize.Visible = false;
			// 
			// nudLightenIntensity
			// 
			this.nudLightenIntensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nudLightenIntensity.Location = new System.Drawing.Point(620, 74);
			this.nudLightenIntensity.Name = "nudLightenIntensity";
			this.nudLightenIntensity.Size = new System.Drawing.Size(74, 20);
			this.nudLightenIntensity.TabIndex = 80;
			this.nudLightenIntensity.Visible = false;
			// 
			// nudDarkenIntensity
			// 
			this.nudDarkenIntensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nudDarkenIntensity.Location = new System.Drawing.Point(620, 74);
			this.nudDarkenIntensity.Name = "nudDarkenIntensity";
			this.nudDarkenIntensity.Size = new System.Drawing.Size(74, 20);
			this.nudDarkenIntensity.TabIndex = 81;
			this.nudDarkenIntensity.Visible = false;
			// 
			// lblIntensity
			// 
			this.lblIntensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblIntensity.AutoSize = true;
			this.lblIntensity.Location = new System.Drawing.Point(565, 76);
			this.lblIntensity.Name = "lblIntensity";
			this.lblIntensity.Size = new System.Drawing.Size(49, 13);
			this.lblIntensity.TabIndex = 82;
			this.lblIntensity.Text = "Intensity:";
			this.lblIntensity.Visible = false;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(709, 491);
			this.Controls.Add(this.lblIntensity);
			this.Controls.Add(this.nudDarkenIntensity);
			this.Controls.Add(this.nudLightenIntensity);
			this.Controls.Add(this.nudDarkenSize);
			this.Controls.Add(this.nudLightenSize);
			this.Controls.Add(this.cbxClearArea);
			this.Controls.Add(this.nudEraserSize);
			this.Controls.Add(this.cbxFillCorners);
			this.Controls.Add(this.nudZoom);
			this.Controls.Add(this.lblZoom);
			this.Controls.Add(this.lblFrame);
			this.Controls.Add(this.tbrFrame);
			this.Controls.Add(this.lblFrameTracker);
			this.Controls.Add(this.lblNoise);
			this.Controls.Add(this.lblSize);
			this.Controls.Add(this.nudPencilNoise);
			this.Controls.Add(this.nudPencilSize);
			this.Controls.Add(this.pbxPreviousColor5);
			this.Controls.Add(this.pbxPreviousColor1);
			this.Controls.Add(this.btnCopyFrame);
			this.Controls.Add(this.lblFPS);
			this.Controls.Add(this.nudFPS);
			this.Controls.Add(this.cbxLoop);
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.btnPrevious);
			this.Controls.Add(this.btnPlayPause);
			this.Controls.Add(this.pbxCurrentColor);
			this.Controls.Add(this.pbxPreviousColor2);
			this.Controls.Add(this.pbxPreviousColor6);
			this.Controls.Add(this.pbxPreviousColor3);
			this.Controls.Add(this.pbxPreviousColor7);
			this.Controls.Add(this.pbxPreviousColor4);
			this.Controls.Add(this.pbxPreviousColor8);
			this.Controls.Add(this.btnEraser);
			this.Controls.Add(this.btnPencil);
			this.Controls.Add(this.btnZoom);
			this.Controls.Add(this.btnSelect);
			this.Controls.Add(this.btnDarken);
			this.Controls.Add(this.btnLighten);
			this.Controls.Add(this.btnCircle);
			this.Controls.Add(this.btnLine);
			this.Controls.Add(this.btnFill);
			this.Controls.Add(this.btnEyedropper);
			this.Controls.Add(this.pbxCanvas);
			this.Controls.Add(this.MenuStrip);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(725, 530);
			this.Name = "frmMain";
			this.Text = "Pixel Art Animation Tool";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmMain_Paint);
			((System.ComponentModel.ISupportInitialize)(this.nudPencilNoise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudPencilSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudFPS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxCurrentColor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxPreviousColor8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbxCanvas)).EndInit();
			this.MenuStrip.ResumeLayout(false);
			this.MenuStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbrFrame)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudZoom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudEraserSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudLightenSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudDarkenSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudLightenIntensity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudDarkenIntensity)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblNoise;
		private System.Windows.Forms.Label lblSize;
		private System.Windows.Forms.NumericUpDown nudPencilNoise;
		private System.Windows.Forms.NumericUpDown nudPencilSize;
		private System.Windows.Forms.PictureBox pbxPreviousColor5;
		private System.Windows.Forms.PictureBox pbxPreviousColor1;
		private System.Windows.Forms.Button btnCopyFrame;
		private System.Windows.Forms.Label lblFPS;
		private System.Windows.Forms.NumericUpDown nudFPS;
		private System.Windows.Forms.CheckBox cbxLoop;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Button btnPrevious;
		private System.Windows.Forms.Button btnPlayPause;
		private System.Windows.Forms.PictureBox pbxCurrentColor;
		private System.Windows.Forms.PictureBox pbxPreviousColor2;
		private System.Windows.Forms.PictureBox pbxPreviousColor6;
		private System.Windows.Forms.PictureBox pbxPreviousColor3;
		private System.Windows.Forms.PictureBox pbxPreviousColor7;
		private System.Windows.Forms.PictureBox pbxPreviousColor4;
		private System.Windows.Forms.PictureBox pbxPreviousColor8;
		private System.Windows.Forms.Button btnEraser;
		private System.Windows.Forms.Button btnPencil;
		private System.Windows.Forms.Button btnZoom;
		private System.Windows.Forms.Button btnSelect;
		private System.Windows.Forms.Button btnDarken;
		private System.Windows.Forms.Button btnLighten;
		private System.Windows.Forms.Button btnCircle;
		private System.Windows.Forms.Button btnLine;
		private System.Windows.Forms.Button btnFill;
		private System.Windows.Forms.Button btnEyedropper;
		private System.Windows.Forms.PictureBox pbxCanvas;
		private System.Windows.Forms.MenuStrip MenuStrip;
		private System.Windows.Forms.ToolStripMenuItem tsmiFile;
		private System.Windows.Forms.ToolStripMenuItem tsmiNew;
		private System.Windows.Forms.ToolStripMenuItem tsmiOpen;
		private System.Windows.Forms.ToolStripMenuItem tsmiOpenPixelAnimation;
		private System.Windows.Forms.ToolStripMenuItem tsmiOpenTracingImage;
		private System.Windows.Forms.ToolStripMenuItem tsmiOpenIndividualFrame;
		private System.Windows.Forms.ToolStripMenuItem tsmiSave;
		private System.Windows.Forms.ToolStripMenuItem tsmiSaveAs;
		private System.Windows.Forms.ToolStripMenuItem tsmiSaveAsSpriteSheet;
		private System.Windows.Forms.ToolStripMenuItem tsmiSaveAsFolder;
		private System.Windows.Forms.ToolStripMenuItem tsmiSelection;
		private System.Windows.Forms.ToolStripMenuItem tsmiCut;
		private System.Windows.Forms.ToolStripMenuItem tsmiCopy;
		private System.Windows.Forms.ToolStripMenuItem tsmiPaste;
		private System.Windows.Forms.ToolStripMenuItem tsmiRotate;
		private System.Windows.Forms.ToolStripMenuItem tsmiRotate90;
		private System.Windows.Forms.ToolStripMenuItem tsmiRotate180;
		private System.Windows.Forms.ToolStripMenuItem tsmiRotate270;
		private System.Windows.Forms.ToolStripMenuItem tsmiFlip;
		private System.Windows.Forms.ToolStripMenuItem tsmiFlipHorizontal;
		private System.Windows.Forms.ToolStripMenuItem tsmiFlipVertical;
		private System.Windows.Forms.ToolStripMenuItem tsmiHelp;
		private System.Windows.Forms.Timer Timer;
		private System.Windows.Forms.ColorDialog dlgColor;
		private System.Windows.Forms.OpenFileDialog dlgOpenFile;
		private System.Windows.Forms.SaveFileDialog dlgSaveFile;
		private System.Windows.Forms.Label lblFrameTracker;
		private System.Windows.Forms.TrackBar tbrFrame;
		private System.Windows.Forms.Label lblFrame;
		private System.Windows.Forms.Label lblZoom;
		private System.Windows.Forms.NumericUpDown nudZoom;
		private System.Windows.Forms.CheckBox cbxFillCorners;
		private System.Windows.Forms.NumericUpDown nudEraserSize;
		private System.Windows.Forms.CheckBox cbxClearArea;
		private System.Windows.Forms.NumericUpDown nudLightenSize;
		private System.Windows.Forms.NumericUpDown nudDarkenSize;
		private System.Windows.Forms.NumericUpDown nudLightenIntensity;
		private System.Windows.Forms.NumericUpDown nudDarkenIntensity;
		private System.Windows.Forms.Label lblIntensity;
	}
}

