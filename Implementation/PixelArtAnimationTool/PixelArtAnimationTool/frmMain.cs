﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

/*
 Created 19/08/2019
 set up tab indices
 make it more obvious which tool is active
 fill tool buggy - edge of frame isn't a 'wall'
 fix fps - cap seems like 10? 
 determine display function is super slow

menu options
	- save as folder		-- create folder				\
															 > check ok is clicked in dialog box, check path exists
	- save as sprite sheet	-- remove gaps between frames	/
	- saving code text
	- save (calls the save as function if the file has not yet been created)
	- open (3 variants)
	- new (with dialog box) - allow for any size
	- selection options
	- help window

set up tools
	- line - needs 🡔 🡕 🡗 (for when change in x is greater than change in y and vice versa)
	- circle
	- select
	- zoom

 set up tool option section - changes depending on tool selected
		- pencil: size, noise
		- eraser: size
			- eyedropper:
			- fill: fill corners?, clear area?
	- line: thickness
	- circle: fill circle?
		- lighten: size, intensity
		- darken: size, intensity
	- select:
	- zoom:	scalant, in/out

	https://stackoverflow.com/questions/9035016/how-can-i-create-a-message-box-that-captures-users-response
	https://docs.microsoft.com/en-us/dotnet/framework/winforms/controls/how-to-save-files-using-the-savefiledialog-component
	https://www.geeksforgeeks.org/how-to-extract-filename-from-a-given-path-in-c-sharp/
	https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm

	 TO LOOK AT - https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/file-system/how-to-create-a-file-or-folder

	CHANGES FROM PLAN
	- frame tracker
	- frame track bar
	- zoom display
*/

namespace PixelArtAnimationTool
{
	public partial class frmMain : Form
	{
		Bitmap Backdrop = null;
		Bitmap Display = null;
		List<Bitmap> Frames;
		int current_frame = 0;

		int canvas_width = 0;
		int canvas_height = 0;

		Color current_color = Color.Black;
		int pixel_size = 16;

		bool drawing = false;
		bool play_button = true;

		List<PictureBox> previous_colors;
		//List<Button> tool_buttons;
		List<Control> tool_options; 

		Vector2 last_lightened_pixel = new Vector2(-1, -1);
		Vector2 last_darkened_pixel = new Vector2(-1, -1);

		Vector2 line_start_point = new Vector2(-1, -1);
		Vector2 line_end_point = new Vector2(-1, -1);
		List<Vector2> line = new List<Vector2>();

		enum tool
		{
			pencil,
			eraser,
			eyedropper,
			fill,
			line,
			circle,
			lighten,
			darken,
			select,
			zoom
		}

		tool current_tool = tool.pencil;

		private void draw_pixel(int a_x, int a_y, Bitmap a_frame, Color? a_color = null)
		{
			if (canvas_width < a_x + pixel_size || canvas_height < a_y + pixel_size)
			{
				return;
			}

			if (a_color == null)
			{
				a_color = current_color;
			}

			for (int row_index = 0; row_index < pixel_size; ++row_index)
			{
				for (int column_index = 0; column_index < pixel_size; ++column_index)
				{
					a_frame.SetPixel(a_x + column_index, a_y + row_index, a_color.Value);
				}
			}

			pbxCanvas.Invalidate();
		}

		private void determine_display_pixel(int a_x, int a_y)
		{
			Color display_color = (Frames[current_frame].GetPixel(a_x, a_y).A > 0) ? Frames[current_frame].GetPixel(a_x, a_y) : Backdrop.GetPixel(a_x, a_y);
			draw_pixel(a_x, a_y, Display, display_color);
		}

		private void determine_display()
		{
			for (int height_index = 0; height_index < canvas_height; height_index += pixel_size)
			{
				for (int width_index = 0; width_index < canvas_width; width_index += pixel_size)
				{
					determine_display_pixel(width_index, height_index);
				}
			}
		}

		private void new_frame()
		{
			Bitmap new_frame = new Bitmap(canvas_width, canvas_height);
			Frames.Add(new_frame);
		}

		private bool is_empty(Bitmap a_frame)
		{
			for (int height_index = 0; height_index < canvas_height; height_index += pixel_size)
			{
				for (int width_index = 0; width_index < canvas_width; width_index += pixel_size)
				{
					if (a_frame.GetPixel(width_index, height_index).A > 0)
					{
						return false;
					}
				}
			}
			return true;
		}

		private void copy_frame(Bitmap a_source, Bitmap a_destination)
		{
			for (int height_index = 0; height_index < canvas_height; height_index += pixel_size)
			{
				for (int width_index = 0; width_index < canvas_width; width_index += pixel_size)
				{
					draw_pixel(width_index, height_index, a_destination, a_source.GetPixel(width_index, height_index));
				}
			}
		}

		private Color modify_color(Color a_source, float a_modifier)
		{
			if (a_source.A == 0)
			{
				return Color.Transparent;
			}

			if (a_modifier > 1.0f && a_source.R == 0 && a_source.G == 0 && a_source.B == 0)
			{
				return Color.FromArgb((int)(25 * a_modifier), (int)(25 * a_modifier), (int)(25 * a_modifier));
			}

			int new_red = (a_source.R * a_modifier > 255) ? 255 : (int)(a_source.R * a_modifier);
			int new_green = (a_source.G * a_modifier > 255) ? 255 : (int)(a_source.G * a_modifier);
			int new_blue = (a_source.B * a_modifier > 255) ? 255 : (int)(a_source.B * a_modifier);
			return Color.FromArgb(new_red, new_green, new_blue);
		}

		private void toggle_controls()
		{
			foreach (Control control in Controls)
			{
				if (control.GetType() != typeof(Label) &&
					control != btnPlayPause &&
					control != nudFPS &&
					control != cbxLoop &&
					control != MenuStrip)
				{
					control.Enabled = !control.Enabled;
				}
			}
		}

		public frmMain()
		{
			InitializeComponent();
			canvas_height = pbxCanvas.Height;
			canvas_width = pbxCanvas.Width;

			Backdrop = new Bitmap(canvas_width, canvas_height);
			Display = new Bitmap(canvas_width, canvas_height);

			Frames = new List<Bitmap> { };
			new_frame();

			previous_colors = new List<PictureBox>
			{
				pbxPreviousColor1,
				pbxPreviousColor2,
				pbxPreviousColor3,
				pbxPreviousColor4,
				pbxPreviousColor5,
				pbxPreviousColor6,
				pbxPreviousColor7,
				pbxPreviousColor8
			};

			//tool_buttons = new List<Button>
			//{
			//	btnPencil,
			//	btnEraser,
			//	btnEyedropper,
			//	btnFill,
			//	btnLine,
			//	btnCircle,
			//	btnLighten,
			//	btnDarken,
			//	btnSelect,
			//	btnZoom
			//};

			tool_options = new List<Control>
			{
				lblSize,
				lblNoise,
				lblIntensity,
				nudPencilSize,
				nudPencilNoise,
				nudEraserSize,
				cbxFillCorners,
				cbxClearArea,
				nudLightenSize,
				nudLightenIntensity,
				nudDarkenSize,
				nudDarkenIntensity
			};

			Color backdrop_color = Color.LightGray;
			for (int height_index = 0; height_index < canvas_height; height_index += pixel_size)
			{
				for (int width_index = 0; width_index < canvas_width; width_index += pixel_size)
				{
					backdrop_color = (height_index % (pixel_size * 2) == width_index % (pixel_size * 2)) ? Color.LightGray : Color.DarkGray;
					draw_pixel(width_index, height_index, Backdrop, backdrop_color);
				}
			}

			copy_frame(Backdrop, Display);
		}

		private void frmMain_Paint(object sender, PaintEventArgs e)
		{
			Graphics graphics = e.Graphics;
			pbxCanvas.Image = Display;
		}

		private void start_drawing(object sender, MouseEventArgs e)
		{
			drawing = true;
			Draw(sender, e);
		}

		private void stop_drawing(object sender, MouseEventArgs e)
		{
			drawing = false;

			switch (current_tool)
			{
				case tool.line:
					for (int index = 0; index < line.Count; ++index)
					{
						draw_pixel((int)line[index].X, (int)line[index].Y, Frames[current_frame]);
					}
					line_start_point.X = -1;
					line_start_point.Y = -1;
					line_end_point.X = -1;
					line_end_point.Y = -1;
					break;

				case tool.lighten:
					last_lightened_pixel.X = -1;
					last_lightened_pixel.Y = -1;
					break;

				case tool.darken:
					last_darkened_pixel.X = -1;
					last_darkened_pixel.Y = -1;
					break;
			}
		}

		private void use_pencil(int a_x, int a_y)
		{
			draw_pixel(a_x, a_y, Frames[current_frame]);
			determine_display_pixel(a_x, a_y);
		}

		private void use_eraser(int a_x, int a_y)
		{
			draw_pixel(a_x, a_y, Frames[current_frame], Color.Transparent);
			determine_display_pixel(a_x, a_y);
		}

		private void use_eyedropper(int a_x, int a_y)
		{
			if (Frames[current_frame].GetPixel(a_x, a_y).A > 0)
			{
				set_color(Frames[current_frame].GetPixel(a_x, a_y));
			}
		}

		private void use_fill(int a_x, int a_y)
		{
			// create a list that will contain the pixels to be checked
			List<Vector2> open_list = new List<Vector2>();
			// create a list that will contain the pixels that will be filled
			List<Vector2> valid_pixels = new List<Vector2>();

			// add the starting pixel to the open list
			open_list.Add(new Vector2(a_x,a_y));

			// create a Vector2 called current_pixel which will store the location of the pixel currently being looked at
			Vector2 current_pixel = new Vector2();

			// loop while the open list isn't empty
			while (open_list.Count > 0)
			{
				// set 'current_pixel' to store the pixel at the start of the open list
				current_pixel = open_list[0];
				// remove the first pixel in the open list
				open_list.Remove(current_pixel);
				// add the current pixel to the valid pixel list
				valid_pixels.Add(current_pixel);

				// create a list called adjacent pixels which contains all of the pixels next to the current pixel
				List<Vector2> adjacent_pixels = new List<Vector2>();
				adjacent_pixels.Add(new Vector2(current_pixel.X, (current_pixel.Y + pixel_size)));
				adjacent_pixels.Add(new Vector2((current_pixel.X + pixel_size), current_pixel.Y));
				adjacent_pixels.Add(new Vector2(current_pixel.X, (current_pixel.Y - pixel_size)));
				adjacent_pixels.Add(new Vector2((current_pixel.X - pixel_size), current_pixel.Y));

				// the "fill corners" checkbox was checked
				if (cbxFillCorners.Checked)
				{
					// add the pixels positioned one space diagonally to the current pixel to the adjacent pixels list
					adjacent_pixels.Add(new Vector2((current_pixel.X - pixel_size), (current_pixel.Y + pixel_size)));
					adjacent_pixels.Add(new Vector2((current_pixel.X + pixel_size), (current_pixel.Y + pixel_size)));
					adjacent_pixels.Add(new Vector2((current_pixel.X - pixel_size), (current_pixel.Y - pixel_size)));
					adjacent_pixels.Add(new Vector2((current_pixel.X + pixel_size), (current_pixel.Y - pixel_size)));
				}

				// loop for each pixel in the adjacent pixels list
				for (int index = 0; index < adjacent_pixels.Count; ++index)
				{
					// if the adjacent pixel is within the bounds of the canvas and is not in the valid pixel list
					// or the open list and has the same colour as the origin pixel
					if ((adjacent_pixels[index].X >= 0) && (adjacent_pixels[index].Y >= 0) &&
						(adjacent_pixels[index].X < canvas_width) && (adjacent_pixels[index].Y < canvas_height) &&
						(!valid_pixels.Contains(adjacent_pixels[index])) &&
						(!open_list.Contains(adjacent_pixels[index])) &&
						compare_colours(Frames[current_frame].GetPixel(a_x,a_y),
										Frames[current_frame].GetPixel((int)adjacent_pixels[index].X, (int)adjacent_pixels[index].Y)))
					{
						// add the pixel to the open list
						open_list.Add(adjacent_pixels[index]);
					}
				}
			}

			// loop through each pixel in the valid pixels list
			for (int index = 0; index < valid_pixels.Count; ++index)
			{
				// if the clear area checkbox is checked
				if (cbxClearArea.Checked)
				{
					// remove the color of the pixel on the current frame
					draw_pixel((int)valid_pixels[index].X, (int)valid_pixels[index].Y, Frames[current_frame], Color.Transparent);
				}
				// otherwise, the clear area checkbox is not checked
				else
				{
					// colour the pixel on the current frame with the current colour
					draw_pixel((int)valid_pixels[index].X, (int)valid_pixels[index].Y, Frames[current_frame]);
				}
			}

			// determine what the display should be
			determine_display();

		}

		private void use_line(int a_x, int a_y)
		{
			for (int index = 0; index < line.Count; ++index)
			{
				determine_display_pixel((int)line[index].X, (int)line[index].Y);
			}
			line.Clear();

			// if either value stored in the line start point Vector2 is -1, the start point has not yet been defined
			if (line_start_point.X == -1 || line_start_point.Y == -1)
			{
				// set the start point to be the current mouse position
				line_start_point.X = a_x;
				line_start_point.Y = a_y;
				// set the end point to be the same point as the mouse position
				line_end_point = line_start_point;
			}
			// otherwise, there is already a start position defined
			else
			{
				// set the end point to be the current mouse position
				line_end_point.X = a_x;
				line_end_point.Y = a_y;
			}

			///////////////////////////////////////////////////////////////////////////////////////////////////////
			//line_start_point.X = 160;
			//line_start_point.Y = 160;
			//line_end_point.X = 320;
			//line_end_point.Y = 176;
			///////////////////////////////////////////////////////////////////////////////////////////////////////

			Vector2 current_pixel = line_start_point;
			int pixel_count = 1;
			if (line_end_point == line_start_point) // .
			{
				line.Add(current_pixel);
			}													  	   
			else if (line_end_point.X == line_start_point.X && line_end_point.Y < line_start_point.Y) // 🡑
			{
				pixel_count += ((int)line_start_point.Y - (int)line_end_point.Y) / pixel_size;
				for (int index = 0; index < pixel_count; ++index)
				{
					line.Add(current_pixel);
					current_pixel.Y -= pixel_size;
				}
			}
			else if (line_end_point.X == line_start_point.X && line_end_point.Y > line_start_point.Y) // 🡓
			{
				pixel_count += ((int)line_end_point.Y - (int)line_start_point.Y) / pixel_size;
				for (int index = 0; index < pixel_count; ++index)
				{
					line.Add(current_pixel);
					current_pixel.Y += pixel_size;
				}
			}
			else if (line_end_point.X < line_start_point.X && line_end_point.Y == line_start_point.Y) // 🡐
			{
				pixel_count += ((int)line_start_point.X - (int)line_end_point.X) / pixel_size;
				for (int index = 0; index < pixel_count; ++index)
				{
					line.Add(current_pixel);
					current_pixel.X -= pixel_size;
				}
			}
			else if (line_end_point.X > line_start_point.X && line_end_point.Y == line_start_point.Y) // 🡒
			{
				pixel_count += ((int)line_end_point.X - (int)line_start_point.X) / pixel_size;
				for (int index = 0; index < pixel_count; ++index)
				{
					line.Add(current_pixel);
					current_pixel.X += pixel_size;
				}
			}
			else if (line_end_point.X > line_start_point.X && line_end_point.Y > line_start_point.Y) // 🡖
			{
				if ((line_end_point.X - line_start_point.X) >= (line_end_point.Y - line_start_point.Y))
				{
					pixel_count += ((int)line_end_point.X - (int)line_start_point.X) / pixel_size;
					for (int index = 0; index < pixel_count; ++index)
					{
						line.Add(current_pixel);
						current_pixel.X += pixel_size;
						current_pixel.Y = (((line_end_point.Y - line_start_point.Y) / (line_end_point.X - line_start_point.X)) *
										  (current_pixel.X - line_start_point.X) + line_start_point.Y);

						current_pixel.Y = (int)((current_pixel.Y + (pixel_size / 2)) / pixel_size);
						current_pixel.Y *= pixel_size;
					}
				}
				else
				{
					pixel_count += ((int)line_end_point.Y - (int)line_start_point.Y) / pixel_size;
					for (int index = 0; index < pixel_count; ++index)
					{
						line.Add(current_pixel);
						current_pixel.Y += pixel_size;
						current_pixel.X = (((line_end_point.X - line_start_point.X) / (line_end_point.Y - line_start_point.Y)) *
										  (current_pixel.Y - line_start_point.Y) + line_start_point.X);

						current_pixel.X = (int)((current_pixel.X + (pixel_size / 2)) / pixel_size);
						current_pixel.X *= pixel_size;
					}
				}
			} 
			else if (line_end_point.X > line_start_point.X && line_end_point.Y < line_start_point.Y) // 🡕
			{

			}
			else if (line_end_point.X < line_start_point.X && line_end_point.Y > line_start_point.Y) // 🡗
			{

			}
			else if (line_end_point.X < line_start_point.X && line_end_point.Y < line_start_point.Y) // 🡔
			{

			}

			for (int index = 0; index < line.Count; ++index)
			{
				draw_pixel((int)line[index].X, (int)line[index].Y, Display);
			}
		}

		private void use_circle() { }

		private void use_lighten(int a_x, int a_y)
		{
			if (last_lightened_pixel.X != a_x || last_lightened_pixel.Y != a_y)
			{
				draw_pixel(a_x, a_y, Frames[current_frame], modify_color(Frames[current_frame].GetPixel(a_x, a_y), 1.1f));
				determine_display_pixel(a_x, a_y);
				last_lightened_pixel.X = a_x;
				last_lightened_pixel.Y = a_y;
			}
		}

		private void use_darken(int a_x, int a_y)
		{
			if (last_darkened_pixel.X != a_x || last_darkened_pixel.Y != a_y)
			{
				draw_pixel(a_x, a_y, Frames[current_frame], modify_color(Frames[current_frame].GetPixel(a_x, a_y), 0.9f));
				determine_display_pixel(a_x, a_y);
				last_darkened_pixel.X = a_x;
				last_darkened_pixel.Y = a_y;
			}
		}

		private void use_select() { }
		private void use_zoom() { }

		private void Draw(object sender, MouseEventArgs e)
		{
			if (drawing &&
				e.X >= 0 && e.X < canvas_width &&
				e.Y >= 0 && e.Y < canvas_height)
			{
				int pixel_x = e.X;
				int pixel_y = e.Y;

				pixel_x /= pixel_size;
				pixel_x *= pixel_size;

				pixel_y /= pixel_size;
				pixel_y *= pixel_size;

				switch (current_tool)
				{
					case tool.pencil:
						use_pencil(pixel_x, pixel_y);
						break;

					case tool.eraser:
						use_eraser(pixel_x, pixel_y);
						break;

					case tool.eyedropper:
						use_eyedropper(pixel_x, pixel_y);
						break;

					case tool.fill:
						use_fill(pixel_x, pixel_y);
						break;

					case tool.line:
						use_line(pixel_x, pixel_y);
						break;

					case tool.circle:
						break;

					case tool.lighten:
						use_lighten(pixel_x, pixel_y);
						break;

					case tool.darken:
						use_darken(pixel_x, pixel_y);
						break;

					case tool.select:
						break;

					case tool.zoom:
						break;
				}
			}
		}

		private void btnNext_Click(object sender, EventArgs e)
		{
			// if this is the last frame
			if (current_frame == Frames.Count - 1)
			{
				// create a new frame
				new_frame();
				// adjust the track bar to show the correct amount of frames
				tbrFrame.Maximum = Frames.Count();
				// switch to the new frame
				++current_frame;
				// set the display to be the background as the new frame is empty
				copy_frame(Backdrop, Display);
			}
			// if this is not the last frame
			else
			{
				// switch to the next frame
				++current_frame;
				// determine what should appear on the display
				determine_display();
			}

			lblFrameTracker.Text = (current_frame + 1).ToString() + "/" + Frames.Count.ToString();
			tbrFrame.Value = current_frame + 1;
		}

		private void btnPrevious_Click(object sender, EventArgs e)
		{
			// if this is the first frame
			if (current_frame == 0)
			{
				// return as there is no previous frame to this frame
				return;
			}
			// otherewise, if this is the last frame and it is empty
			else if (current_frame == Frames.Count - 1 && is_empty(Frames[current_frame]))
			{
				// remove this frame
				Frames.RemoveAt(current_frame);
				// adjust the track bar to show the correct amount of frames
				tbrFrame.Maximum = Frames.Count();
			}
			// go to the previous frame
			--current_frame;
			// determie what should appear on the display
			determine_display();

			lblFrameTracker.Text = (current_frame + 1).ToString() + "/" + Frames.Count.ToString();
			tbrFrame.Value = current_frame + 1;
		}

		private void btnCopyFrame_Click(object sender, EventArgs e)
		{
			if (current_frame == 0)
			{
				return;
			}
			else if (!is_empty(Frames[current_frame]))
			{
				DialogResult copy_warning = MessageBox.Show(
											"Are you sure you want to replace the contents of the current frame with the previous frame?",
											"Warning: Attempted Copy Over Pre-Existing Frame",
											MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
				if (copy_warning == DialogResult.Yes)
				{
					copy_frame(Frames[current_frame - 1], Frames[current_frame]);
					determine_display();	
				}
			}
			else
			{
				copy_frame(Frames[current_frame - 1], Frames[current_frame]);
				determine_display();
			}
		}

		private bool compare_colours(Color a_color1, Color a_color2)
		{
			if (a_color1.A == 0 && a_color2.A == 0)
			{
				return true;
			}
			else if (a_color1.A == a_color2.A &&
				a_color1.R == a_color2.R &&
				a_color1.G == a_color2.G &&
				a_color1.B == a_color2.B)
			{
				return true;
			}
			return false;
		}

		private void set_color(Color a_color)
		{
			// if the argument colour is not already the current colour
			if (!compare_colours(a_color,current_color))
			{
				// loop for each of the previous colour picture boxes
				for (int search_index = 0; search_index < previous_colors.Count; ++search_index)
				{
					// if the current picture box holds the argument colour, or if this is the last picture box
					if (compare_colours(a_color, previous_colors[search_index].BackColor) || search_index == previous_colors.Count - 1)
					{
						// loop backwards through the picture boxes, starting at the current picture box
						for (int shift_index = search_index; shift_index > 0; --shift_index)
						{
							// set the current picture box's colour to be the previous picture box' s colour
							previous_colors[shift_index].BackColor = previous_colors[shift_index - 1].BackColor;
						}
						// set the first picture box's colour to by the current colour
						previous_colors[0].BackColor = current_color;

						// set the current colour to be the argument colour
						current_color = a_color;
						// set the current colour picture box to show the new current colour
						pbxCurrentColor.BackColor = current_color;
						// set the colour stored in the color dialog to be the current color so the user will have that as a base when definining a custom color
						dlgColor.Color = current_color;
						// exit this function
						return;
					}
				}
				
			}
		}

		private void choose_color(object sender, MouseEventArgs e)
		{
			dlgColor.ShowDialog();
			set_color(dlgColor.Color);

		}

		private void pbxPreviousColor1_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor1.BackColor);
		}

		private void pbxPreviousColor2_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor2.BackColor);
		}

		private void pbxPreviousColor3_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor3.BackColor);
		}

		private void pbxPreviousColor4_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor4.BackColor);
		}

		private void pbxPreviousColor5_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor5.BackColor);
		}

		private void pbxPreviousColor6_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor6.BackColor);
		}

		private void pbxPreviousColor7_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor7.BackColor);
		}

		private void pbxPreviousColor8_Click(object sender, EventArgs e)
		{
			set_color(pbxPreviousColor8.BackColor);
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			Timer.Interval = 1000 / (int)nudFPS.Value;
			if (current_frame == Frames.Count - 1)
			{
				if (cbxLoop.Checked)
				{
					current_frame = 0;
				}
				else
				{
					Timer.Stop();
					toggle_controls();
					tbrFrame.Value = current_frame + 1;
					play_button = true;
					btnPlayPause.Text = "Play";
				}
			}
			else
			{
				++current_frame;
			}

			determine_display();
			lblFrameTracker.Text = (current_frame + 1).ToString() + "/" + Frames.Count.ToString();
		}

		private void btnPlayPause_Click(object sender, EventArgs e)
		{
			if (play_button)
			{
				if (!cbxLoop.Checked)
				{
					current_frame = 0;
				}

				determine_display();
				Timer.Start();
				btnPlayPause.Text = "Pause";
			}
			else
			{
				Timer.Stop();
				tbrFrame.Value = current_frame + 1;
				btnPlayPause.Text = "Play";
			}
			toggle_controls();
			play_button = !play_button;
		}

		private void btnPencil_Click(object sender, EventArgs e)
		{
			current_tool = tool.pencil;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}

			lblSize.Visible = true;
			nudPencilSize.Visible = true;
			lblNoise.Visible = true;
			nudPencilNoise.Visible = true;
		}

		private void btnEraser_Click(object sender, EventArgs e)
		{
			current_tool = tool.eraser;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}

			lblSize.Visible = true;
			nudEraserSize.Visible = true;
		}

		private void btnEyedropper_Click(object sender, EventArgs e)
		{
			current_tool = tool.eyedropper;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}
		}

		private void btnFill_Click(object sender, EventArgs e)
		{
			current_tool = tool.fill;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}

			cbxFillCorners.Visible = true;
			cbxClearArea.Visible = true;
		}

		private void btnLine_Click(object sender, EventArgs e)
		{
			current_tool = tool.line;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}
		}

		private void btnCircle_Click(object sender, EventArgs e)
		{
			current_tool = tool.circle;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}
		}

		private void btnLighten_Click(object sender, EventArgs e)
		{
			current_tool = tool.lighten;
			last_lightened_pixel.X = -1;
			last_lightened_pixel.Y = -1;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}

			lblSize.Visible = true;
			nudLightenSize.Visible = true;
			lblIntensity.Visible = true;
			nudLightenIntensity.Visible = true;
		}

		private void btnDarken_Click(object sender, EventArgs e)
		{
			current_tool = tool.darken;
			last_darkened_pixel.X = -1;
			last_darkened_pixel.Y = -1;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}

			lblSize.Visible = true;
			nudDarkenSize.Visible = true;
			lblIntensity.Visible = true;
			nudDarkenIntensity.Visible = true;
		}

		private void btnSelect_Click(object sender, EventArgs e)
		{
			current_tool = tool.select;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}
		}

		private void btnZoom_Click(object sender, EventArgs e)
		{
			current_tool = tool.zoom;

			foreach (Control option in tool_options)
			{
				option.Visible = false;
			}
		}

		private void tbrFrame_Scroll(object sender, EventArgs e)
		{
			current_frame = tbrFrame.Value - 1;
			lblFrameTracker.Text = (current_frame + 1).ToString() + "/" + Frames.Count.ToString();
			determine_display();
		}

		private void tsmiSaveAsFolder_Click(object sender, EventArgs e)
		{
			// if the animation is running
			if (!play_button)
			{
				// trigger the playpause button click event to pause the animation
				btnPlayPause_Click(sender, e);
			}

			// set the filters of the save file dialog to be png and jpg
			dlgSaveFile.Filter = "PNG Image|*.png|JPEG Image|*.jpg";
			// set the title of the save file dialog
			dlgSaveFile.Title = "Save A Folder Containing an Image of Each Frame";
			// set the default file name to be "untitled"
			dlgSaveFile.FileName = "untitled";
			// show the save file dialog to the user
			dlgSaveFile.ShowDialog();

			if (/*ok clicked*/true)
			{
				// loop through each frame starting at the end
				for (int index = Frames.Count - 1; index > 1; --index)
				{
					// if the frame is empty
					if (is_empty(Frames[index]))
					{
						// remove this frame
						Frames.RemoveAt(index);
						// adjust the track bar to show the correct amount of frames
						tbrFrame.Maximum = Frames.Count();
					}
					// otherwise, the frame was not empty
					else
					{
						// exit the loop
						break;
					}
				}

				//////////////////////////////// check path exists /////////////////////////////////////////////
				

				// create the string "FolderLocation" which stores the location that the folder will appear
				string FolderLocation =  Path.GetDirectoryName(dlgSaveFile.FileName);

				// create the string "SaveType" which will store whether the frames will be saved as jpgs or pngs
				string SaveType = null;

				switch (dlgSaveFile.FilterIndex)
				{
					// if the user chose to save the frames as jpgs
					case 1:
						// set SaveType to store the PNG image format
						SaveType = ".png";
						break;
					// if the user chose to save the frames as pngs
					case 2:
						// set SaveType to store the JPG image format
						SaveType = ".jpg";
						break;
				}

				///////////////////////// create folder /////////////////////////////////
				


				// loop through each frame
				for (int index = 0; index < Frames.Count; ++index)
				{
					// save this frame with the name of its position in the animation
					Frames[index].Save(FolderLocation + "\\Frame" + (index + 1).ToString() + SaveType);
				}
			}
		}

		private void tsmiSaveAsSpriteSheet_Click(object sender, EventArgs e)
		{
			// if the animation is running
			if (!play_button)
			{
				// trigger the playpause button click event to pause the animation
				btnPlayPause_Click(sender, e);
			}

			// set the filters of the save file dialog to be png and jpg
			dlgSaveFile.Filter = "PNG Image|*.png|JPEG Image|*.jpg";
			// set the title of the save file dialog
			dlgSaveFile.Title = "Save As a Sprite Sheet";
			// set the default file name to be "untitled"
			dlgSaveFile.FileName = "untitled";
			// make the save file dialog add the extension to the file name if the user did not (.jpg or .png)
			dlgSaveFile.AddExtension = true;
			// show the save file dialog to the user
			dlgSaveFile.ShowDialog();

			if (/*ok clicked*/true)
			{
				// loop through each frame starting at the end
				for (int index = Frames.Count - 1; index > 1; --index)
				{
					// if the frame is empty
					if (is_empty(Frames[index]))
					{
						// remove this frame
						Frames.RemoveAt(index);
						// adjust the track bar to show the correct amount of frames
						tbrFrame.Maximum = Frames.Count();
					}
					// otherwise, the frame was not empty
					else
					{
						// exit the loop
						break;
					}
				}

				//////////////////////////////// check path exists /////////////////////////////////////////////


				// create the string "SaveLocation" which stores the location that the folder will appear
				string SaveLocation = Path.GetDirectoryName(dlgSaveFile.FileName);

				Bitmap SpriteSheet = new Bitmap(canvas_width * Frames.Count, canvas_height);

				for (int frame_index = 0; frame_index < Frames.Count; ++frame_index)
				{
					for (int height_index = 0; height_index < canvas_height; ++height_index)
					{
						for (int width_index = 0; width_index < canvas_width; ++width_index)
						{
							SpriteSheet.SetPixel((canvas_width * frame_index) + width_index, height_index, Frames[frame_index].GetPixel(width_index, height_index));
						}
					}
				}

				SpriteSheet.Save(dlgSaveFile.FileName);
			}
		}
	}
}
