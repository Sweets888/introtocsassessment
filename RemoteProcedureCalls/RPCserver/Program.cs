﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;

namespace RPCserver
{

	class Program
	{
		static void Main(string[] args)
		{
			int port = 8080;
			string PlayerName = "Player";

			TcpServerChannel channel = new TcpServerChannel(port);	// open a tcp server channel using the port 8080
			ChannelServices.RegisterChannel(channel, false);		// register the channel "channel" without encryption

			RemotingConfiguration.RegisterWellKnownServiceType		// register player class as something that can be remotely accessed by a client
				(
					typeof(Player),									// type of class being registered
					PlayerName,										// name of the class being registered
					WellKnownObjectMode.SingleCall					// Whether each network message is being processed by a new object or a singleton
				);

			Console.WriteLine("Listening for requests.\nPress enter to exit...\n");
			Console.ReadLine();
		}
	}
}

class Player : MarshalByRefObject
{
	public void SayHello(string username, string text)
	{
		if (text == "quit")
		{
			Console.WriteLine(username + "quit");
		}
		else
		{
			Console.WriteLine("[" + username + "]: " + text);
		}
	}
}