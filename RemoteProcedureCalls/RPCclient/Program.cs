﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;

namespace RPCclient
{
	class Program
	{
		static void Main(string[] args)
		{
			// open network channel
			// get a reference to the server's player object
			// remotely call function
			int port = 8080;
			string PlayerName = "Player";
			string Username = "User1";

			TcpClientChannel channel = new TcpClientChannel();						// Establish network channel
			ChannelServices.RegisterChannel(channel, false);						// Register the channel "channel" without encryption

			string playerURL = "tcp://localhost:" + port + "/" + PlayerName;		// URL of the Player Object
			Player player = (Player)Activator.GetObject(typeof(Player), playerURL); // Get Reference to the Player Object on the Server

			string text = "";
			while (text != "quit")
			{
				Console.Write("Type a message to the server or type 'quit' to exit\n");
				text = Console.ReadLine();
				player.SayHello(Username, text);									// Calls the function on the Server rather than the Client
			}
		}
	}
}

class Player : MarshalByRefObject
{
	public void SayHello(string username, string text)
	{
		if (text == "quit")
		{
			Console.WriteLine(username + "quit");
		}
		else
		{
			Console.WriteLine("[" + username + "]: " + text);
		}
	}
}