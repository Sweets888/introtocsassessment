/* INSTRUCTIONS
	
	The following code is to be used in a 2D AIE bootstrap project

	Create a new header file (.h) and source file (.cpp)
	Copy the code under the subheading "// HEADER FILE" into the header file 
	Copy the code under the subheading "// SOURCE FILE" into the source file
	
	In the source file, near the top of the page, write the name of the header file in the comment and code marked with the following /* Write Header File Name Here*/ so the header file can be included in the source file

	The following values can be changed in the header file:
	 - m_x_position (a float representing the horizontal coordinate representing where the animation will appear on screen)
	 - m_y_position (a float representing the vertical coordinate representing where the animation will appear on screen)
	 - m_loop (a boolean variable which dictates whether the animation will repeat once it has finished or not)

	The following values can be changed in the source file:
	 - m_fps (a float representing the frame rate that the animation will run at (frames per second))

	In the app header file:
	 - include the header file for your animation. e.g #include "MyAnimation.h"
	 - create a pointer to the class created in your animations header file. e.g MyAnimation* m_sprite;

	In the app source file:
	 - allocate memory to the pointer in the "startup" function. e.g m_sprite = new MyAnimation();
	 - delete the contents of the pointer in the "shutdown" function. e.g delete m_sprite;
	 - set the pointer to point to null in the "shutdown" function. e.g m_sprite = nullptr;
	 - call the "update_animation" function for your animation in the "update" function, passing in "deltaTime". e.g m_sprite->update_animation(deltaTime);
	 - call the "draw" function for your animation in the "draw"function, passing in "m_2dRenderer". e.g m_sprite->draw(m_2dRenderer);
*/

// HEADER FILE

/*-----------------------------------------------------------------
	File Name:
	Purpose:
	Author:
	Created:
	Last Modified:
	Note: Created with the Pixel Art Animation Tool by Michael Sweetman
-----------------------------------------------------------------*/

// if PixelAnimationFolder has not yet been defined
#ifndef _PixelAnimationFolder_
// define PixelAnimationFolder
#define _PixelAnimationFolder_

// include 'Texture.h' in order to assign the textures representing the frames of the animation
#include "Texture.h"
// include "Renderer2D.h" in order to draw the animation on screen
#include "Renderer2D.h"

class {[FolderName]}
{
public:

	// @Brief	constructor
	{[FolderName]}();

	// @Brief	deconstructor
	~{[FolderName]}();

	/*
		@Brief	determines the texture which should be used for the next frame
		@Param	'a_delta_time':	the amount of time passed since the previous frame
	*/
	void update_animation(float a_delta_time);

	/*
		@Brief	draws the current frame on screen
		@Param	'a_2dRenderer':	an object of the Renderer2D class responsible for drawing the animation on screen
	*/
	void draw(aie::Renderer2D* a_2dRenderer);

private:

	// 'm_x_position':		a float representing the horizontal coordinate representing where the animation will appear on screen
	float					m_x_position = 640.0f;
	// 'm_y_position':		a float representing the vertical coordinate representing where the animation will appear on screen
	float					m_y_position = 360.0f;

	// 'm_fps':				a float representing the frame rate that the animation will run at (frames per second)
	static const float		m_fps;
	// 'm_frame_count':		an integer representing the amount of frames in the animation
	static const int		m_frame_count = {[FrameCount]};
	// 'm_animation_timer': a float that stores how much time has passed since the current frame was last changed
	float					m_animation_timer = 0.0f;
	// 'm_current_frame':	an integer representing the index of the frame of the animation that will be displayed on screen
	int						m_current_frame = 0;
	// 'm_frames':			an array of texture pointers which stores all of the frames that make up the animation
	aie::Texture*			m_frames[m_frame_count];
	// 'm_loop':			a boolean variable which dictates whether the animation will repeat once it has finished or not
	bool					m_loop = {[LoopCheckboxState]};
};

#endif

// SOURCE FILE

/*-----------------------------------------------------------------
	File Name:
	Purpose:
	Author:
	Created:
	Last Modified:
	Note: Created with the Pixel Art Animation Tool by Michael Sweetman
-----------------------------------------------------------------*/

// include "/* Write Header File Name Here */" as the functions and variables that will be defined in this file were declared there
#include /* Write Header File Name Here (Inside Quotes " ")*/

// set the frame rate of the animation to {[fps]} fps
const float PixelAnimation_Folder::m_fps = {[fps]}.0f;

// @Brief	constructor
{[FolderName]}::{[FolderName]}()
{
	// fill 'm_frames' with pointers to the images representing the frames of the animation
	m_frames[0] = new aie::Texture("./textures/{[FolderName]}/{[index]}.png");
}

// @Brief	deconstructor
{[FolderName]}::~{[FolderName]}()
{
	// loop for each frame in the 'm_frames' array
	for (aie::Texture* a_frame : m_frames)
	{
		// delete the current frame from memory
		delete a_frame;
		// set the pointer that was pointing to the current frame to instead point to null
		a_frame = nullptr;
	}
}

/*
	@Brief	determines the texture which should be used for the next frame
	@Param	'a_delta_time':	the amount of time passed since the previous frame
*/
void {[FolderName]}::update_animation(float a_delta_time)
{
	// increase the animation timer by delta time (the amount of time passed since the last frame)
	m_animation_timer += a_delta_time;

	// if the of amount time passed since the previous frame changed has exceeded the amount of time dictated by the fps (1/fps)
	if (m_animation_timer >= 1.0f / m_fps)
	{
		// if the current frame is the last frame
		if (m_current_frame == m_frame_count - 1)
		{
			// if the animation should loop
			if (m_loop)
			{
				// set the current frame to be the first frame
				m_current_frame = 0;
			}
		}
		// otherwise, the current frame is not the last frame
		else
		{
			// make the current frame the next frame
			++m_current_frame;
		}
		// set the animation timer to 0
		m_animation_timer = 0.0f;
	}
}

/*
	@Brief	draws the current frame on screen
	@Param	'a_2dRenderer':	an object of the Renderer2D class responsible for drawing the animation on screen
*/
void {[FolderName]}::draw(aie::Renderer2D* a_2dRenderer)
{
	// draw the current frame on screen at the coordinates specified by 'm_x_position' and 'm_y_position'
	a_2dRenderer->drawSprite(m_frames[m_current_frame], m_x_position, m_y_position);
}