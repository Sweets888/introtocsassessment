/*-----------------------------------------------------------------
	File Name: main.cpp
	Purpose: The start point of the program that runs the application
	Author: Michael Sweetman
	Created: 07/08/2019
	Last Modified: 12/08/2019
-----------------------------------------------------------------*/

// include 'AnimationPlaybackApp.h' in order to run the application
#include "AnimationPlaybackApp.h"
// include "crtdbg.h" in order to check for memory leaks
#include <crtdbg.h>

int main()
{
	// set up the debug flag used to detect memory leaks
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	// create app and set it to be a new object of the 'AnimationPlaybackApp' class
	auto app = new AnimationPlaybackApp();

	// run the application with the title "Pixel Art Animation Test", the dimensions of 1280 and 720 and not in fullscreen
	app->run("Pixel Art Animation Test", 1280, 720, false);

	// delete app from memory
	delete app;

	// exit the program
	return 0;
}