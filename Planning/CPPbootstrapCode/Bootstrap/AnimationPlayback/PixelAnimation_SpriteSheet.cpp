/*-----------------------------------------------------------------
	File Name: PixelAnimation_SpriteSheet.cpp
	Purpose: A class which manages the cycle of an animation made with a Sprite Sheet
	Author: Michael Sweetman
	Created: 12/08/2019
	Last Modified: 13/08/2019
-----------------------------------------------------------------*/

// include "PixelAnimation_Folder.h" as the functions and variables that will be defined in this file were declared there
#include "PixelAnimation_SpriteSheet.h"

// set the frame rate of the animation to 1 fps
const float PixelAnimation_SpriteSheet::m_fps = 1.0f;

// @Brief	constructor
PixelAnimation_SpriteSheet::PixelAnimation_SpriteSheet()
{
	// set the 'm_sprite_sheet' texture pointer to point to the sprite sheet file
	m_sprite_sheet = new aie::Texture("./textures/numbers.png");
}

// @Brief	deconstructor
PixelAnimation_SpriteSheet::~PixelAnimation_SpriteSheet()
{
	// delete the sprite sheet from memory
	delete m_sprite_sheet;
	// set the pointer that was pointing to the sprite sheet to instead point to null
	m_sprite_sheet = nullptr;
}

/*
	@Brief	determines the area of the sprite sheet to draw on screen
	@Param	'a_delta_time':	the amount of time passed since the previous frame
*/
void PixelAnimation_SpriteSheet::update_animation(float a_delta_time)
{
	// increase the animation timer by delta time (the amount of time passed since the last frame)
	m_animation_timer += a_delta_time;

	// if the amount of time passed since the previous frame changed has exceeded the amount of time dictated by the fps (1/fps)
	if (m_animation_timer >= 1.0f / m_fps)
	{
		// if the current frame is the last frame
		if (m_current_frame == m_frame_count - 1)
		{
			// if the animation should loop
			if (m_loop)
			{
				// set the current frame to be the first frame
				m_current_frame = 0;
			}
		}
		// otherwise, the current frame is not the last frame
		else
		{
			// make the current frame the next frame
			++m_current_frame;
		}
		// set the animation timer to 0
		m_animation_timer = 0.0f;
	}
}

/*
	@Brief	draws the needed region of the sprite sheet on screen
	@Param	'a_2dRenderer':	an object of the Renderer2D class responsible for drawing the animation on screen
*/
void PixelAnimation_SpriteSheet::draw(aie::Renderer2D* a_2dRenderer)
{
	// set the bounds of the sprite sheet that should be drawn to only contain the current frame
	a_2dRenderer->setUVRect(m_current_frame / float(m_frame_count), 0.0f, 1.0f / m_frame_count, 1.0f);
	// draw the sprite sheet on screen at the coordinates specified by 'm_x_position' and 'm_y_position'
	a_2dRenderer->drawSprite(m_sprite_sheet, m_x_position, m_y_position,m_sprite_sheet->getHeight(),m_sprite_sheet->getHeight());
	// reset the bounds the the renderer draws with to encompass the whole image
	a_2dRenderer->setUVRect(0.0f, 0.0f, 1.0f, 1.0f);
}