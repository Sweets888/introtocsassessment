/*-----------------------------------------------------------------
	File Name: AnimationPlayback.h
	Purpose: To test the classes that manage the cycling of an animation (for the Pixel Art Animation Tool)
	Author: Michael Sweetman
	Created: 07/08/2019
	Last Modified: 12/08/2019
-----------------------------------------------------------------*/

// if AnimationPlaybackApp has not yet been defined
#ifndef _AnimationPlaybackApp_
// define AnimationPlaybackApp
#define _AnimationPlayBackApp_

// include "Application.h" as this will manage startup and shutdown, properly executing update and draw functions and ensure the game runs properly as an application
#include "Application.h"
// include "Renderer2D.h" in order to draw the textures on screen
#include "Renderer2D.h"
// include "PixelAnimation_Folder.h" as this will manage the cycle of animations made with a set of images
#include "PixelAnimation_Folder.h"
// include "PixelAnimation_SpriteSheet.h" as this will manage the cycle of animations made with a single Sprite Sheet
#include "PixelAnimation_SpriteSheet.h"

class AnimationPlaybackApp : public aie::Application
{
public:

	/*
		@Brief	the function which will trigger when the game begins, defining the initial conditions of each variable and object
		@Return	true if the start up sequence was successful, false otherwise
	*/
	virtual bool startup();

	/*
		@Brief	the function which will trigger when the game finishes, deleting any data stored in the heap
	*/
	virtual void shutdown();

	/*
		@Brief	manages the changes between frames as well as managing inputs
		@Param	'deltaTime' the amount of time passed since the previous frame
	*/
	virtual void update(float deltaTime);

	/*
		@Brief	draws the textures on screen
	*/
	virtual void draw();

protected:
	// 'm_2dRenderer':				a pointer to an object of the "Renderer2D" class which will be used to draw textures, text and shapes on screen
	aie::Renderer2D*				m_2dRenderer;
	// 'm_walker':					a pointer to an object of the "PixelAnimation_Folder" class which will draw a walking character on screen
	PixelAnimation_Folder*			m_walker;
	// 'm_numbers':					a point to an object of the "PixelAnimation_SpriteSheet" class which will draw numbered squares on screen
	PixelAnimation_SpriteSheet*		m_numbers;
};

#endif