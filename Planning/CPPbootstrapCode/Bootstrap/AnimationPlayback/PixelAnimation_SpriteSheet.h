/*-----------------------------------------------------------------
	File Name: PixelAnimation_SpriteSheet.h
	Purpose: A class which manages the cycle of an animation made with a Sprite Sheet
	Author: Michael Sweetman
	Created: 12/08/2019
	Last Modified: 12/08/2019
-----------------------------------------------------------------*/

// if PixelAnimationSpriteSheet has not yet been defined
#ifndef _PixelAnimationSpriteSheet_
// define PixelAnimationSpriteSheet
#define _PixelAnimationSpriteSheet_

// include 'Texture.h' in order to assign the texture representing the frames of the animation
#include "Texture.h"
// include "Renderer2D.h" in order to draw the animation on screen
#include "Renderer2D.h"

class PixelAnimation_SpriteSheet
{
public:

	// @Brief	constructor
	PixelAnimation_SpriteSheet();

	// @Brief	deconstructor
	~PixelAnimation_SpriteSheet();

	/*
		@Brief	determines the area of the sprite sheet to draw on screen
		@Param	'a_delta_time':	the amount of time passed since the previous frame
	*/
	void update_animation(float a_delta_time);

	/*
		@Brief	draws the needed region of the sprite sheet on screen
		@Param	'a_2dRenderer':	an object of the Renderer2D class responsible for drawing the animation on screen
	*/
	void draw(aie::Renderer2D* a_2dRenderer);

private:

	// 'm_x_position':		a float representing the horizontal coordinate representing where the animation will appear on screen
	float					m_x_position = 640.0f;
	// 'm_y_position':		a float representing the vertical coordinate representing where the animation will appear on screen
	float					m_y_position = 360.0f;

	// 'm_fps':				a float representing the frame rate that the animation will run at (frames per second)
	static const float		m_fps;
	// 'm_frame_count':		an integer representing the amount of frames in the animation
	static const int		m_frame_count = 8;
	// 'm_animation_timer': a float that stores how much time has passed since the current frame was last changed
	float					m_animation_timer = 0.0f;
	// 'm_current_frame':	an integer representing the index of the frame of the animation that will be displayed on screen
	int						m_current_frame = 0;
	// 'm_sprite_sheet'		a pointer to the texture which stores all of the frames of the animation
	aie::Texture*			m_sprite_sheet;
	// 'm_loop':			a boolean variable which dictates whether the animation will repeat once it has finished or not
	bool					m_loop = true;
};

#endif