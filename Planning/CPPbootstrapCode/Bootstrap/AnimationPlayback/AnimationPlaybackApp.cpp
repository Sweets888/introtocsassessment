/*-----------------------------------------------------------------
	File Name: AnimationPlayback.cpp
	Purpose: To test the classes that manage the cycling of an animation (for the Pixel Art Animation Tool)
	Author: Michael Sweetman
	Created: 07/08/2019
	Last Modified: 12/08/2019
-----------------------------------------------------------------*/

// include "AnimationPlaybackApp.h" as this is where the functions and variables that will be defined in this file were declared
#include "AnimationPlaybackApp.h"
// include 'Input.h' in order to interpret inputs from the user
#include "Input.h"

/*
	@Brief	the function which will trigger when the game begins, defining the initial conditions of each variable and object
	@Return	true if the start up sequence was successful, false otherwise
*/
bool AnimationPlaybackApp::startup()
{
	// set 'm_2dRenderer' to be a new 2D renderer stored in the heap
	m_2dRenderer = new aie::Renderer2D();
	// set 'm_walker' to be a new PixelAnimation_Folder object
	m_walker = new PixelAnimation_Folder();
	// set 'm_walker' to be a new PixelAnimation_SpriteSheet object
	m_numbers = new PixelAnimation_SpriteSheet();
	
	// return true to indicate that the startup was successful
	return true;
}

/*
	@Brief	the function which will trigger when the game finishes, deleting any data stored in the heap
*/
void AnimationPlaybackApp::shutdown()
{
	// delete the Renderer2D object pointed to by 'm_2dRenderer' from the heap
	delete m_2dRenderer;
	// set the 'm_2dRenderer' pointer to point to null
	m_2dRenderer = nullptr;

	// delete the 'PixelAnimation_Folder' object pointed to by 'm_walker' from the heap
	delete m_walker;
	// set the 'm_walker' pointer to point to null
	m_walker = nullptr;

	// delete the 'PixelAnimation_SpriteSheet' object pointed to by 'm_numbers'
	delete m_numbers;
	// set the 'm_numbers' pointer to point to null
	m_numbers = nullptr;
}

/*
	@Brief	manages the changes between frames as well as managing inputs
	@Param	'deltaTime' the amount of time passed since the previous frame
*/
void AnimationPlaybackApp::update(float deltaTime)
{
	// create a new pointer to an object of the input class to read the input of the user (use a pre-existing one if one exists)
	aie::Input* input = aie::Input::getInstance();

	// update the animation of the walker sprite using deltaTime
	m_walker->update_animation(deltaTime);
	// update the animation of the numbers sprite using deltaTime
	m_numbers->update_animation(deltaTime);

	// if the escape key is down
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
	{
		// quit the game at the end of this frame
		quit();
	}
}

/*
	@Brief	draws the textures on screen
*/
void AnimationPlaybackApp::draw()
{
	// clear the screen at the start of the frame
	clearScreen();

	// start drawing
	m_2dRenderer->begin();

	// draw the walker with the 2dRenderer
	m_walker->draw(m_2dRenderer);
	// draw the number with the 2dRenderer
	m_numbers->draw(m_2dRenderer);

	// stop drawing
	m_2dRenderer->end();
}