/*-----------------------------------------------------------------
	File Name: PixelAnimation_Folder.cpp
	Purpose: A class which manages the cycle of an animation made up of a series of images
	Author: Michael Sweetman
	Created: 12/08/2019
	Last Modified: 12/08/2019
-----------------------------------------------------------------*/

// include "PixelAnimation_Folder.h" as the functions and variables that will be defined in this file were declared there
#include "PixelAnimation_Folder.h"

// set the frame rate of the animation to 16 fps
const float PixelAnimation_Folder::m_fps = 16.0f;

// @Brief	constructor
PixelAnimation_Folder::PixelAnimation_Folder()
{
	// fill 'm_frames' with pointers to the images representing the frames of the animation
	m_frames[0] = new aie::Texture("./textures/1.png");
	m_frames[1] = new aie::Texture("./textures/2.png");
	m_frames[2] = new aie::Texture("./textures/3.png");
	m_frames[3] = new aie::Texture("./textures/4.png");
	m_frames[4] = new aie::Texture("./textures/5.png");
	m_frames[5] = new aie::Texture("./textures/6.png");
	m_frames[6] = new aie::Texture("./textures/7.png");
	m_frames[7] = new aie::Texture("./textures/8.png");
}

// @Brief	deconstructor
PixelAnimation_Folder::~PixelAnimation_Folder()
{
	// loop for each frame in the 'm_frames' array
	for (aie::Texture* a_frame : m_frames)
	{
		// delete the current frame from memory
		delete a_frame;
		// set the pointer that was pointing to the current frame to instead point to null
		a_frame = nullptr;
	}
}

/*
	@Brief	determines the texture which should be used for the next frame
	@Param	'a_delta_time':	the amount of time passed since the previous frame
*/
void PixelAnimation_Folder::update_animation(float a_delta_time)
{
	// increase the animation timer by delta time (the amount of time passed since the last frame)
	m_animation_timer += a_delta_time;

	// if the of amount time passed since the previous frame changed has exceeded the amount of time dictated by the fps (1/fps)
	if (m_animation_timer >= 1.0f / m_fps)
	{
		// if the current frame is the last frame
		if (m_current_frame == m_frame_count - 1)
		{
			// if the animation should loop
			if (m_loop)
			{
				// set the current frame to be the first frame
				m_current_frame = 0;
			}
		}
		// otherwise, the current frame is not the last frame
		else
		{
			// make the current frame the next frame
			++m_current_frame;
		}
		// set the animation timer to 0
		m_animation_timer = 0.0f;
	}
}

/*
	@Brief	draws the current frame on screen
	@Param	'a_2dRenderer':	an object of the Renderer2D class responsible for drawing the animation on screen
*/
void PixelAnimation_Folder::draw(aie::Renderer2D* a_2dRenderer)
{
	// draw the current frame on screen at the coordinates specified by 'm_x_position' and 'm_y_position'
	a_2dRenderer->drawSprite(m_frames[m_current_frame], m_x_position, m_y_position);
}