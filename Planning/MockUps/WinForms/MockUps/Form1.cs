﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MockUps
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void helpToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Form2 Help = new Form2();
			Help.Show();
		}

		private void button17_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Are you sure you want to replace the contents of the current frame with the previous frame?", "Warning: Attempted Copy Over Pre-Existing Frame", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Form3 New = new Form3();
			New.Show();
		}

		private void button11_Click(object sender, EventArgs e)
		{

		}

		private void button12_Click(object sender, EventArgs e)
		{

		}

		private void button1_Click(object sender, EventArgs e)
		{

		}

		private void button2_Click(object sender, EventArgs e)
		{

		}

		private void button3_Click(object sender, EventArgs e)
		{

		}

		private void button4_Click(object sender, EventArgs e)
		{

		}

		private void button5_Click(object sender, EventArgs e)
		{

		}

		private void button6_Click(object sender, EventArgs e)
		{

		}

		private void button7_Click(object sender, EventArgs e)
		{

		}

		private void button10_Click(object sender, EventArgs e)
		{

		}
	}
}
